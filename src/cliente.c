#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>

#define BUFLEN 128
#define QLEN 10

#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 256
#endif

#define QUEUESIZE 10

int main(int argc, char *argv[]){
	

	if(argc== 1){
		printf("Uso:/servidor<numero de puerto><interfaz>\n");
		exit(-1);
	}
	if(argc==3){
		printf("por favor especificar un numero de puerto e interfaz\n");
		}

	int puerto_server+ atoi(argv[1]);
	struc sockaddr_in direccion_server;

	memset(&direccion_server,0,sizeof(direccion_server));

	direccion_server.sin_family = AD_INET;
	direccion_server.sin_port= htons(puerto_server);
	
	direccion_server.sim_addr.s_addr=inet_addr(argv[2]);

	int sockd;
	
	printf("Bienvenido Cliente");
	printf("Establecio una conexion con el servidor");
	
	if((sockd= socket(direccion_server.sin_family, SOCK_STREAM, 0))< 0){
		perror("Error al conectar\n");
		return -1;
	}

	int resp = connect(sockd, (struct sockaddr*)&direccion_server, sizeof(direccion_server));

	if(resp < 0){
		perror("Error al conectar\n");
		close(sockd);
		return 0;
	}

	int env= write(sockd, argv[3],400);
	printf("Se enviaron %d bytes\n",env);

	char buf[1000] = {0};
	int rec=0;


	int destino= open(argv[4],O_WRONLY|O_CREAT|O_TRUNC,0666);

	if(destino < 0){
		perro("Error al copiar el archivo con la ruta enviada\n");
		close(sockd);
		return-1;
	}

	unsigned long bytesTotalesDescargados = 0;
	unsigned long recC;

	while((recC= read(sockd, buf, 1000)) > 0){
		bytesTotalesDescargados += rec;
		write(destino,buf,rec);
	}

	printf("La cantidad total de Bytes descargados; %id \n",bytesTottalesDescargados);
	close(sockd);
	close(destino);
}
