#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <stddef.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <netdb.h>
#include <errno.h>
#include <syslog.h>
#include <sys/socket.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/resource.h>


#define BUFLEN 128
#define QLEN 10

#ifndef HOST_NAME_MAX
#define HOST_NAME_MAX 256
#endif

#define QUEUESIZE 1


int main(int argc, char *argv[]){
	if(argc != 3){
		printf("Error_Sintaxis: ./servidor [Puerto] [Interfaz]\n");
				exit(-1);
	}
	int port= atoi(argv[1]);

	struct sockaddr_in direccion_servidor;
	memset(&direccion_servidor, 0, sizeof(direccion_servidor));

	direccion_servidor.sin_family= AF_INET;
	direccion_servidor.sin_port= htons(port);

	direccion_servidor.sin_addr.s_addr = inet_addr(argv[2]);

	int fd;

	if((fd=socket(direccion_servidor.sin_family, SOCK_STREAM, 0))<0){
		perror("Error creando descriptor de socket\n");
		return -1;
	}


	if(bind(fd, (struct sockaddr *)&direccion_servidor, sizeof(direccion_servidor))<0){
		perror("Error en el bind\n");
		return -1;
	}

	if(listen(fd, QUEUESIZE)<0){
		perror("Error listen\n");
	}

	while(1){
		int sockfd_conectado = accept(fd, NULL, 0);
                printf("Un cliente se ha conectado\n");

                char buf[300] = {0};

                int leidos = read(sockfd_conectado, buf, 300);

                if(leidos < 0){
                        perror("Error! No se logro leer\n");
                }
                else{
			umask(0);

		        int fuente = open(buf,O_RDONLY);
		        if(fuente < 0){
		                perror("Ruta no encontrada \n");
               			exit(1);
		        }

			unsigned char *buffer= malloc( sizeof(unsigned char)*10000);
		        memset(buffer,0,10000);
		        ssize_t bytesLeidos = 0;
		        int Bytes=0;
		        Bytes+=bytesLeidos;
			do{
		                bytesLeidos = read(fuente,buffer,10000);
		                Bytes+=bytesLeidos;
		                write(sockfd_conectado,buffer,bytesLeidos);
		                memset(buffer,0,10000);
		        }while (bytesLeidos > 0);
			free(buffer);
			close(fuente);
		        printf("%d Bytes cargados.\n",Bytes);
                }

                close(sockfd_conectado);
	}

	return 0;
}
