all: servidor cliente

create:
	mkdir -p bin obj

servidor: servidorc
	gcc obj/servidor.o -o ./bin/servidor
cliente: clientec
	gcc obj/cliente.o -o ./bin/cliente
servidorc: src/servidor.c
	gcc -c src/servidor.c -o obj/servidor.o
clientec: src/cliente.c
	gcc -c src/cliente.c -o obj/cliente.o
clear:
	rm obj/*
